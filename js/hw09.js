//Функция, которая возвращает объект и выводит результат в консоль
function getcardsObject(cards) {
    let cardsObject = {};

    cards.forEach(function (card) {
        let bgImg = card.querySelector('[data-card-image]');
        let bgImgUrl = bgImg.style.backgroundImage.slice(5, -1).replace(/"/g, '');

        let cardObject = {
            "title": card.querySelector('[data-card-topic]').innerHTML,
            "description": card.querySelector('[data-card-subtopic]').innerHTML,
            "date": card.querySelector('[data-card-date]').innerHTML,
            "image": bgImgUrl,
            "label": card.querySelector('[data-card-label]').innerHTML
        }

        let dataGroup = card.dataset.group;

        if (!(dataGroup in cardsObject)) {
            cardsObject[dataGroup] = [];
        }

        cardsObject[dataGroup].push(cardObject);
    });

    console.log(cardsObject);
}

getcardsObject(allExistingCards);