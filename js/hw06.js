function changeCardInfo() {
    const mainCard = document.querySelectorAll('.main__card-description');

    mainCard.forEach(item => {

        //Перевод текстового содержимого заголовка карточки в верхний регистр
        let cardTitle = item.querySelector('.main__card-topic');
        let titleText = cardTitle.textContent;

        cardTitle.innerHTML = titleText.toUpperCase();

        //Обрезка строки до 20 символов и добавление многоточия
        let cardDescr = item.querySelector('.main__card-subtopic');
        let descrText = cardDescr.textContent.trim();

        if (descrText.length > 20) {
            cardDescr.innerHTML = `${descrText.slice(0, 20)}...`;
        }
    })
}

changeCardInfo();