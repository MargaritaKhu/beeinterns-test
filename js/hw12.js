/*1.*/
const greetPerson = () => {
    const sayName = (name) => {
        return `hi, i am ${name}`
    }
    return sayName
}

const greeting = greetPerson();

console.log(greeting('Pavel'));
/*Выведется строка 'hi, i am Pavel', тк функция greetPerson вызывает функциюьsayName 
и возвращает ее значение, в качестве аргумента которой передается строка 'Pavel'*/

console.log(greeting('Irina'));
/*Выведется строка 'hi, i am Irina', тк функция greetPerson вызывает функцию sayName
и возвращает ее значение, в качестве аргумента которой передается строка 'Irina'*/



/*2)*/
let y1 = 'test';
const foo1 = () => {
    //var newItem = 'hello';
    console.log(y);
}
foo1();
/*Выведется значение переменной строка 'test', тк этой переменной нет
в локальной области видимости, будет использована глобальная переменная*/
console.log(newItem);
/*Выведется ошибка, поскольку локальная переменная newImage доступна только внутри функции*/



/*3.*/
let y = 'test';
let test = 2;
const foo2 = () => {
    const test = 5;
    const bar = () => {
        console.log(5 + test);
    }
    bar()
}
foo2();
/*Выведется 10, тк в качестве значения переменной test в функции bar 
будет использоваться 5 - первое найденное значение, после которого поиск остановится*/



/*4.*/
const bar = () => {
    const b = 'no test'
}
bar();

const foo = (() => {
    console.log(b);
})();
const b = 'test';
/*После вызова функции foo выведется ошибка, тк переменная b со значением 'no test'
определяется локально в другой функции, а переменная b со значением 'test' определяется после
вызова функции foo => функция foo не имеет к этим переменным доступа*/