//Формирование блока "All lections"
let allExistingCards = document.querySelectorAll('[data-group]');
let newCardsBlock = document.getElementById('all-cards');
let sumLections = document.getElementById('sum-lections');

allExistingCards.forEach(card => {
    newCardsBlock.innerHTML += card.outerHTML;
});

sumLections.innerHTML = allExistingCards.length + ' лекций';

//Фильтрация
let filters = document.querySelectorAll('[data-filter]');

filters.forEach(function (filter) {
    filter.addEventListener('click', function () {

        const parent = newCardsBlock.parentNode;
        let cards = parent.querySelectorAll('.main__card');

        cards.forEach(card => {
            if (filter.dataset.filter === 'sum' || card.dataset.group === filter.dataset.filter) {
                card.style.display = '';
            } else {
                card.style.display = 'none';
            }
        })
    })
})
